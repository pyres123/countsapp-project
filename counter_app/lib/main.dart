import 'dart:async';

import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    
    //calling app main from here
    return MaterialApp(
      title: 'Stack Count App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Counts App'),
    );
  }
}

//stateful widget called on home page
class MyHomePage extends StatefulWidget{
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title; // => Flutter Home Demo Page

  @override
  _MyHomePageState createState() => new _MyHomePageState();

}

class _MyHomePageState extends State<MyHomePage>{
  int _counter = 0;
  String result = "Hello";

  void _incrementCounter(){
    setState(() {
          _counter++;
        });
  }

  void _decrementCounter(){
    setState(() {
          _counter--;
        });
  }

  void _refreshCounter(){
    setState(() {
          _counter = 0;
        });
  }

  Future _scanQR() async{
    try{
      String qrResult = await BarcodeScanner.scan();
    setState(() {
          result = qrResult;
          _counter++;
        });
    }on PlatformException catch (ex){
      if(ex.code == BarcodeScanner.CameraAccessDenied){
        result = "Camera access was denied";
      }else{
        setState(() {
                  result = "Unknown error $ex";
                });
      }
    } on FormatException{
      setState(() {
              result = "You pressed back button before scanning";
            });
      }catch(ex){
        setState(() {
                  result="Unknown error $ex";
                });
      }
  }

  @override
Widget build(BuildContext context){
  return new Scaffold(
    appBar: new AppBar(
      title: new Text(widget.title),
    ),
    body: new Center(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            
            //increment button
            FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: _incrementCounter,
              tooltip: 'Increment',
            ),

            //decrement button
            FloatingActionButton(
              child: Icon(Icons.remove),
              onPressed: _decrementCounter,
              tooltip: 'Decrement',
            ),

            new Text(
              // Text takes a String as it's first argument.
              // We're passing in the value of the counter
              // as an interpolated String.
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),

            //refresh button
            FloatingActionButton(
              child: Icon(Icons.refresh),
              onPressed: _refreshCounter,
              tooltip: 'Refresh',
            ),

            FloatingActionButton(
              child: Icon(Icons.camera_alt),
              onPressed: _scanQR,
            ),
          ],
      ),
    ),

    );
  }
}
